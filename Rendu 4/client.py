import re  # module pour l'expression régulière
import psycopg2

def client(cursor):
    quitter = False
    while not quitter:
        print("1. Ajouter un client")
        print("2. Modifier un client")
        print("3. Supprimer un client")
        print("4. Retour au menu principal")
        choix = input("Choisissez une option : ")

        if choix == '1':
            # Ajouter un client
            telephone_ok = False
            while not telephone_ok:
                try:
                    # On vérifie si le numéro de téléphone existe à l'aide de la fonction
                    telephone = int(input("Entrez le numéro de téléphone : "))
                    telephone_ok = verifier_telephone(cursor, telephone)
                    if not telephone_ok:
                        print("Le numéro de téléphone existe déjà.")
                    else:
                        print("Le numéro de téléphone est disponible.")
                except ValueError:
                    print("Veuillez entrer un numéro valide.")
                except psycopg2.Error as e:
                    print(f"Une erreur de base de données s'est produite : {e}")
                    break
            nom = input("Entrez le nom : ")
            prenom = input("Entrez le prénom : ")
            date_naissance = input("Entrez la date de naissance (YYYY-MM-DD) : ")

            if not verifier_date(date_naissance):
                print("Date de naissance invalide.")
                continue

            adresse = input("Entrez votre adresse : ")
            requete = f"""
            INSERT INTO Personne (telephone, nom, prenom, date_naissance, adresse)
            VALUES (%s, %s, %s, TO_DATE(%s, 'YYYY-MM-DD'), %s);
            INSERT INTO Client (telephone)
            VALUES (%s);
            """
            try:
                cursor.execute(requete, (telephone, nom, prenom, date_naissance, adresse, telephone))
                print("Client ajouté avec succès.")
            except psycopg2.Error as e:
                print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")

        elif choix == '2':
            # Modifier un client
            client_ok = False
            while not client_ok:
                try:
                    telephone = int(input("Entrez le numéro de téléphone du client : "))
                    client_ok = verifier_client(cursor, telephone)
                    if not client_ok:
                        print("Le client n'existe pas.")
                except ValueError:
                    print("Veuillez entrer un numéro de téléphone valide.")

            print("1. Modifier le nom")
            print("2. Modifier le prénom")
            modif = input("Votre choix : ")

            if modif == '1':
                nom = input("Entrez le nouveau nom : ")
                requete = "UPDATE Personne SET nom = %s WHERE telephone = %s;"
                try:
                    cursor.execute(requete, (nom, telephone))
                    print("Nom mis à jour avec succès.")
                except psycopg2.Error as e:
                    print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")

            elif modif == '2':
                prenom = input("Entrez le nouveau prénom : ")
                requete = "UPDATE Personne SET prenom = %s WHERE telephone = %s;"
                try:
                    cursor.execute(requete, (prenom, telephone))
                    print("Prénom mis à jour avec succès.")
                except psycopg2.Error as e:
                    print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")

        elif choix == '3':
            # Supprimer un client
            client_ok = False
            while not client_ok:
                try:
                    telephone = int(input("Entrez le numéro de téléphone du client à supprimer : "))
                    client_ok = verifier_client(cursor, telephone)
                    if not client_ok:
                        print("Le client n'existe pas.")
                except ValueError:
                    print("Veuillez entrer un numéro de téléphone valide.")

            if client_ok:
                try:
                    # Récupérer tous les animaux associés au client
                    query_animaux = "SELECT id_animal FROM Animal WHERE proprietaire = %s;"
                    cursor.execute(query_animaux, (telephone,))
                    animaux = cursor.fetchall()

                    # Supprimer toutes les prescriptions associées à ces animaux
                    for animal in animaux:
                        requete_prescriptions = "DELETE FROM Prescription WHERE animal = %s;"
                        cursor.execute(requete_prescriptions, (animal[0],))

                    # Supprimer tous les animaux associés au client
                    requete_animaux = "DELETE FROM Animal WHERE proprietaire = %s;"
                    cursor.execute(requete_animaux, (telephone,))

                    # Supprimer le client de la table Client
                    requete_client = "DELETE FROM Client WHERE telephone = %s;"
                    cursor.execute(requete_client, (telephone,))

                    # Supprimer la personne de la table Personne
                    requete_personne = "DELETE FROM Personne WHERE telephone = %s;"
                    cursor.execute(requete_personne, (telephone,))

                    print("Client, animaux et prescriptions associés supprimés avec succès.")
                except psycopg2.Error as e:
                    print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")

        elif choix == '4':
            quitter = True

        else:
            print("Attention, choisissez 1, 2, 3 ou 4!")

def verifier_client(cursor, telephone):
    query = "SELECT * FROM Client WHERE telephone = %s;"
    try:
        cursor.execute(query, (telephone,))
        result = cursor.fetchone()
        return result is not None
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")
        return False

def verifier_date(date):
    pattern = re.compile(r'^\d{4}-\d{2}-\d{2}$')
    return bool(pattern.match(date))

def verifier_telephone(cursor, telephone):
    query_telephone = "SELECT * FROM Personne WHERE telephone = %s;"
    try:
        cursor.execute(query_telephone, (telephone,))
        result = cursor.fetchone()
        return result is None
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")
        return False
