import re  # module pour l'expression régulière
import psycopg2

def animal(cursor):
    quitter = False
    while not quitter:
        print("1. Ajouter un animal")
        print("2. Modifier un animal")
        print("3. Supprimer un animal")
        print("4. Retour au menu principal")
        choix = input("Choisissez une option : ")

        if choix == '1':
            # Ajouter un animal
            id_animal_ok = False
            while not id_animal_ok:
                try:
                    id_animal = int(input("Entrez l'identifiant de l'animal : "))
                    id_animal_ok = not verifier_animal(cursor, id_animal)
                    if not id_animal_ok:
                        print("L'identifiant de l'animal existe déjà.")
                except ValueError:
                    print("Veuillez entrer un identifiant valide.")

            nom = input("Entrez le nom de l'animal : ")
            espece = input("Entrez l'espèce de l'animal : ")

            # on verifie que le poids de l'animal >0
            poids = 0
            while poids <= 0:
                try:
                    poids = int(input("Entrez le poids de l'animal : "))
                    if poids <= 0:
                        print("Le poids doit être supérieur à 0.")
                except ValueError:
                    print("Veuillez entrer un poids valide.")

            # on verifie que la taille de l'animal >0
            taille = 0
            while taille <= 0:
                try:
                    taille = int(input("Entrez la taille de l'animal : "))
                    if taille <= 0:
                        print("La taille doit être supérieure à 0.")
                except ValueError:
                    print("Veuillez entrer une taille valide.")
            date_naissance = input("Entrez la date de naissance de l'animal (YYYY-MM-DD) : ")
            type_animal = input("Entrez le type de l'animal (mammifère/reptiles/oiseaux/autres) : ").lower()
            telephone_proprietaire = int(input("Entrez le téléphone du propriétaire : "))

            telephone_personnel_ok = False
            while not telephone_personnel_ok:
                try:
                    telephone_personnel = int(input("Entrez le téléphone du personnel associé : "))
                    telephone_personnel_ok = verifier_specialite_personnel(cursor, telephone_personnel, type_animal)
                    if not telephone_personnel_ok:
                        print(f"Le personnel associé n'a pas la spécialité '{type_animal}'. Voici la liste des personnels ayant cette spécialité :")
                        afficher_personnel_par_specialite(cursor, type_animal)
                except ValueError:
                    print("Veuillez entrer un numéro de téléphone valide.")
                except psycopg2.Error as e:
                    print(f"Une erreur de base de données s'est produite : {e}")

            if not verifier_date(date_naissance):
                print("Date de naissance invalide.")
                continue
            if type_animal not in {'mammifère', 'reptiles', 'oiseaux', 'autres'}:
                print("Le type doit être 'mammifère', 'reptiles', 'oiseaux' ou 'autres'.")
                continue

            proprietaire_ok = verifier_client(cursor, telephone_proprietaire)
            personnel_ok = verifier_personnel(cursor, telephone_personnel)

            if not proprietaire_ok:
                print("Le propriétaire n'existe pas.")
                continue
            if not personnel_ok:
                print("Le personnel n'existe pas.")
                continue

            requete = f"""
            INSERT INTO Animal (id_animal, nom, espece, poids, taille, date_naissance, specialite, proprietaire, personnel)
            VALUES (%s, %s, %s, %s, %s, TO_DATE(%s, 'YYYY-MM-DD'), %s, %s, %s);
            """
            try:
                cursor.execute(requete, (id_animal, nom, espece, poids, taille, date_naissance, type_animal, telephone_proprietaire, telephone_personnel))
                print("Animal ajouté avec succès.")
            except psycopg2.Error as e:
                print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")

        elif choix == '2':
            # Modifier un animal
            animal_ok = False
            while not animal_ok:
                try:
                    id_animal = int(input("Entrez l'identifiant de l'animal : "))
                    animal_ok = verifier_animal(cursor, id_animal)
                    if not animal_ok:
                        print("L'animal n'existe pas.")
                except ValueError:
                    print("Veuillez entrer un identifiant valide.")

            print("1. Modifier le poids")
            print("2. Modifier la taille")
            modif = input("Votre choix : ")

            if modif == '1':
                poids = int(input("Entrez le nouveau poids : "))
                requete = "UPDATE Animal SET poids = %s WHERE id_animal = %s;"
                try:
                    cursor.execute(requete, (poids, id_animal))
                    print("Poids mis à jour avec succès.")
                except psycopg2.Error as e:
                    print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")

            elif modif == '2':
                taille = int(input("Entrez la nouvelle taille : "))
                requete = "UPDATE Animal SET taille = %s WHERE id_animal = %s;"
                try:
                    cursor.execute(requete, (taille, id_animal))
                    print("Taille mise à jour avec succès.")
                except psycopg2.Error as e:
                    print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")

        elif choix == '3':
            # Supprimer un animal
            animal_ok = False
            while not animal_ok:
                try:
                    id_animal = int(input("Entrez l'identifiant de l'animal à supprimer : "))
                    animal_ok = verifier_animal(cursor, id_animal)
                    if not animal_ok:
                        print("L'animal n'existe pas.")
                except ValueError:
                    print("Veuillez entrer un identifiant valide.")

            try:
                # Supprimer toutes les prescriptions associées à l'animal
                requete_prescriptions = "DELETE FROM Prescription WHERE animal = %s;"
                cursor.execute(requete_prescriptions, (id_animal,))

                # Supprimer l'animal
                requete_animal = "DELETE FROM Animal WHERE id_animal = %s;"
                cursor.execute(requete_animal, (id_animal,))

                print("Animal et prescriptions associées supprimés avec succès.")
            except psycopg2.Error as e:
                print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")

        elif choix == '4':
            quitter = True
        else:
            print("Attention, choisissez 1, 2, 3 ou 4!")

def verifier_animal(cursor, id_animal):
    query = "SELECT * FROM Animal WHERE id_animal = %s;"
    try:
        cursor.execute(query, (id_animal,))
        result = cursor.fetchone()
        return result is not None
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")
        return False

def verifier_client(cursor, telephone):
    query = "SELECT * FROM Client WHERE telephone = %s;"
    try:
        cursor.execute(query, (telephone,))
        result = cursor.fetchone()
        return result is not None
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")
        return False

def verifier_personnel(cursor, telephone):
    query = "SELECT * FROM Personnel WHERE telephone = %s;"
    try:
        cursor.execute(query, (telephone,))
        result = cursor.fetchone()
        return result is not None
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")
        return False

def verifier_specialite_personnel(cursor, telephone, specialite):
    query = "SELECT specialite FROM Personnel WHERE telephone = %s;"
    try:
        cursor.execute(query, (telephone,))
        result = cursor.fetchone()
        if result and result[0].lower() == specialite.lower():
            return True
        return False
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")
        return False

def afficher_personnel_par_specialite(cursor, specialite):
    query = "SELECT telephone, nom, prenom FROM Personnel WHERE specialite = %s;"
    try:
        cursor.execute(query, (specialite,))
        result = cursor.fetchall()
        for personnel in result:
            print(f"Téléphone: {personnel[0]}, Nom: {personnel[1]}, Prénom: {personnel[2]}")
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")

def verifier_date(date):
    pattern = re.compile(r'^\d{4}-\d{2}-\d{2}$')
    return bool(pattern.match(date))
