import psycopg2

def afficher_total_molecules(cursor):
    try:
        cursor.execute("SELECT SUM(quantite) FROM Prescription;")
        total_quantite = cursor.fetchone()[0]

        if total_quantite is None:
            total_quantite = 0

        print(f"La quantité totale de molécules prescrites par la clinique est de {total_quantite}")
    except psycopg2.Error as e:
        print(f"Erreur lors de la récupération des données: {e}")
