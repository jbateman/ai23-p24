# -*- coding: utf-8 -*-

# Importation des fonctions
from BDD import connect_to_database, close_connection
from prescriptions import prescriptions
from prescriptions_veterinaire import obtenir_prescriptions_par_veterinaire
from medicament import menu_medicaments
from molecules_total import afficher_total_molecules
from poids_taille import poids_taille_moyenne
from client import client
from animal import animal
from personnel import personnel

def main():

    connection, curseur = connect_to_database()

    boucle = True

    if connection == None or curseur == None:
        boucle = False

    while boucle:
        print("\nMenu Principal:\n")
        print("1. Actions sur les Animaux")
        print("2. Actions sur les Clients")
        print("3. Actions sur le Personnel")
        print("4. Actions sur les Médicaments")
        print("5. Actions sur les Prescriptions")
        print("6. Afficher toutes les prescriptions d'un vétérinaire")
        print("7. Afficher la quantité totale de molécules prescrites")
        print("8. Afficher le poids et la taille moyenne des Animaux")
        print("9. Quitter")
        print("\n\n")

        choix = input("Votre choix : ")

        if choix == '1':
            animal(curseur)
        elif choix == '2':
            client(curseur)
        elif choix == '3':
            personnel(curseur)
        elif choix == '4':
            menu_medicaments(curseur)
        elif choix == '5':
            prescriptions(curseur)
        elif choix == '6':
            obtenir_prescriptions_par_veterinaire(curseur)
        elif choix == '7':
            afficher_total_molecules(curseur)
        elif choix == '8':
            poids_taille_moyenne(curseur)
        elif choix == '9':
            boucle = False
        else:
            print("Option invalide. Veuillez choisir une option valide.")

    close_connection(connection, curseur)

if __name__ == "__main__":
    main()
