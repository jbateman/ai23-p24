import re  # module pour l'expression régulière
import psycopg2
import prettytable

def prescriptions(cursor):
    quitter = False
    while not quitter:
        date_ok = False
        print("1. Ajouter une prescription")
        print("2. Modifier une prescription")
        print("3. Supprimer une prescription")
        print("4. Retour au menu principal")
        choix = input("Choisissez une option : ")

        if choix == '1':
            # Ajouter une prescription
            # On renseigne l'identifiant de l'animal
            animal_ok = False
            while not animal_ok:
                try:
                    # On vérifie si l'animal existe à l'aide d'une fonction
                    id_animal = int(input("Entrez l'identifiant de l'animal : "))
                    animal_ok = verifier_animal(cursor, id_animal)
                    if not animal_ok:
                        print("L'animal cherché n'existe pas")
                except ValueError:
                    print("Veuillez entrer un nombre valide.")

            molecule_ok = False
            while not molecule_ok:
                try:
                    # on vérifie si la molécule existe
                    molecule = str(input("Entrez la molécule de la prescription :"))
                    molecule_ok = verifier_molecule(cursor, molecule)
                    if molecule_ok:
                        # On vérifie si la molécule est adaptée à l'animal
                        if verifier_molecule_specialite(cursor, molecule, id_animal):
                            print("Le médicament convient à l'animal.")
                        else:
                            molecule_ok = False
                            print("Le médicament ne convient pas à l'animal")
                    else:
                        print("La molécule n'existe pas")
                except ValueError:
                    print("Veuillez entrer un nom valide.")

            while not date_ok:
                date_debut = input("Entrez la date de début de la prescription : ")
                # On utilise notre fonction de vérification de date ici
                date_ok = verifier_date(date_debut)
                if not date_ok:
                    print("Date invalide. Veuillez réessayer avec le format 'YYYY-MM-DD'")

            duree = int(input("Entrez la durée de la prescription :"))
            quantite = int(input("Entrez la quantité de la molécule à prendre :"))

            veto_ok = False
            while not veto_ok:
                veterinaire = int(input("Entrez le téléphone du vétérinaire associé : "))
                veterinaire_ok = verifier_veterinaire(cursor, veterinaire)
                if veterinaire_ok:
                    if veterinaire_specialite(cursor, veterinaire, id_animal):
                        print("Le vétérinaire a la bonne spécialité")
                        veto_ok = True
                    else:
                        print("Le vétérinaire n'a pas la bonne spécialité")
                else:
                    print("Le vétérinaire n'existe pas")

            id_prescription = id_animal + int(date_debut.replace("-", ""))
            requete = f"INSERT INTO Prescription VALUES('{id_prescription}', TO_DATE('{date_debut}','YYYY-MM-DD'), {duree}, '{molecule}', {quantite}, {id_animal}, '{veterinaire}');"
            cursor.execute(requete)
            print("Prescription ajoutée avec succès.")

        elif choix == '2':
            # Modifier une prescription
            presc_ok = False
            while not presc_ok:
                try:
                    prescription_id = int(input("Entrez l'identifiant de la prescription : "))
                    presc_ok = verifier_prescription(cursor, prescription_id)
                    if not presc_ok:
                        print("La prescription n'existe pas")
                except ValueError:
                    print("Veuillez entrer un identifiant valide.")

            afficher_prescription = "SELECT * FROM Prescription WHERE id_prescription = %s;"
            try:
                cursor.execute(afficher_prescription, (prescription_id,))
                prescription_info = cursor.fetchone()
                if prescription_info:
                    print("Voici les informations de la prescription :")
                    for key, value in zip(cursor.description, prescription_info):
                        print(f"{key[0]}: {value}")

                    # Modification loop
                    modifier = True
                    while modifier:
                        print("\nVous pouvez modifier :")
                        print("1. La durée")
                        print("2. La quantité")
                        modif = input("Votre choix : ")

                        if modif == '1':
                            try:
                                duree2 = int(input("Entrez la nouvelle durée : "))
                                modif_duree = "UPDATE Prescription SET duree = %s WHERE id_prescription = %s;"
                                cursor.execute(modif_duree, (duree2, prescription_id))
                                conn.commit()  # Ensure changes are saved

                                # Fetch and display updated information
                                cursor.execute(afficher_prescription, (prescription_id,))
                                prescription2_info = cursor.fetchone()
                                if prescription2_info:
                                    print("Voici les informations de la prescription mises à jour :")
                                    for key, value in zip(cursor.description, prescription2_info):
                                        print(f"{key[0]}: {value}")
                                else:
                                    print("Aucune information trouvée pour cette prescription.")
                            except psycopg2.Error as e:
                                print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")
                            except ValueError:
                                print("Veuillez entrer une durée valide.")

                        elif modif == '2':
                            try:
                                quantite2 = int(input("Entrez la nouvelle quantité : "))
                                modif_quantite = "UPDATE Prescription SET quantite = %s WHERE id_prescription = %s;"
                                cursor.execute(modif_quantite, (quantite2, prescription_id))
                                conn.commit()  # Ensure changes are saved

                                # Fetch and display updated information
                                cursor.execute(afficher_prescription, (prescription_id,))
                                prescription2_info = cursor.fetchone()
                                if prescription2_info:
                                    print("Voici les informations de la prescription mises à jour :")
                                    for key, value in zip(cursor.description, prescription2_info):
                                        print(f"{key[0]}: {value}")
                                else:
                                    print("Aucune information trouvée pour cette prescription.")
                            except psycopg2.Error as e:
                                print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")
                            except ValueError:
                                print("Veuillez entrer une quantité valide.")

                        else:
                            print("Choix invalide. Veuillez réessayer.")

                        # Ask if the user wants to continue modifying
                        continuer = input("Voulez-vous continuer à modifier ? (oui/non) : ")
                        if continuer.lower() != 'oui':
                            modifier = False
                else:
                    print("Aucune information trouvée pour cette prescription.")
            except psycopg2.Error as e:
                print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")

        elif choix == '3':
            # Supprimer une prescription
            presc_ok = False
            while not presc_ok:
                try:
                    prescription_id = int(input("Entrez l'identifiant de la prescription à supprimer (au format : id_animal + int(date_debut.replace("-", ""))) : "))
                    presc_ok = verifier_prescription(cursor, prescription_id)
                    if not presc_ok:
                        print("La prescription n'existe pas")
                except ValueError:
                    print("Veuillez entrer un identifiant valide.")

            if presc_ok:
                try:
                    requete_suppression = "DELETE FROM Prescription WHERE id_prescription = %s;"
                    cursor.execute(requete_suppression, (prescription_id,))
                    print("Prescription supprimée avec succès.")
                except psycopg2.Error as e:
                    print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")

        elif choix == '4':
            # Quitter
            quitter = True

        else:
            print("Attention, choisissez 1, 2, 3 ou 4!")

def verifier_prescription(cursor, id):
    query_prescription = "SELECT * FROM Prescription WHERE id_prescription = %s;"
    try:
        cursor.execute(query_prescription, (id,))
        result = cursor.fetchone()
        return result is not None
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")
        return False

def verifier_animal(cursor, id):
    query_animal = "SELECT * FROM Animal WHERE id_animal = %s;"
    try:
        cursor.execute(query_animal, (id,))
        result = cursor.fetchone()
        return result is not None
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")
        return False

def verifier_molecule(cursor, molecule):
    query_molecule = "SELECT * FROM Medicament WHERE molecule = %s;"
    try:
        cursor.execute(query_molecule, (molecule,))
        result = cursor.fetchone()
        return result is not None
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")
        return False

def verifier_molecule_specialite(cursor, molecule, id_animal):
    query_molecule_spe1 = "SELECT specialite FROM Medicament WHERE molecule = %s;"
    query_molecule_spe2 = "SELECT specialite FROM Animal WHERE id_animal = %s;"
    try:
        cursor.execute(query_molecule_spe1, (molecule,))
        result_molecule = cursor.fetchone()

        cursor.execute(query_molecule_spe2, (id_animal,))
        result_animal = cursor.fetchone()

        if result_molecule and result_animal:
            return result_molecule[0] == result_animal[0]
        else:
            return False
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")
        return False

def veterinaire_specialite(cursor, veterinaire, id_animal):
    query_veto_spe1 = "SELECT specialite FROM Personnel WHERE telephone = %s;"
    query_veto_spe2 = "SELECT specialite FROM Animal WHERE id_animal = %s;"
    try:
        cursor.execute(query_veto_spe1, (veterinaire,))
        result_veto = cursor.fetchone()

        cursor.execute(query_veto_spe2, (id_animal,))
        result_animal = cursor.fetchone()

        if result_veto and result_animal:
            return result_veto[0] == result_animal[0]
        else:
            return False
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")
        return False

def verifier_veterinaire(cursor, telephone):
    query_personnel = "SELECT poste FROM Personnel WHERE telephone = %s;"
    try:
        cursor.execute(query_personnel, (telephone,))
        result = cursor.fetchone()
        if result is not None:
            return result[0] == "vétérinaire"
        else:
            return False
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")
        return False

def verifier_date(date):
    # Utilisation d'une expression régulière pour vérifier si la date a un format valide
    pattern = re.compile(r'^\d{4}-\d{2}-\d{2}$')
    return bool(pattern.match(date))
