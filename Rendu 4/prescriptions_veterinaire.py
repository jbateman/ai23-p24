import psycopg2

def obtenir_prescriptions_par_veterinaire(cursor):
    # Demande à l'utilisateur d'entrer le numéro de téléphone
    telephone = input("Entrez le numéro de téléphone du vétérinaire : ")

    # Vérifie si le vétérinaire existe dans la base de données
    if not verifier_veterinaire(cursor, telephone):
        print("Le vétérinaire avec ce numéro de téléphone n'existe pas ou n'est pas un vétérinaire.")
        return

    try:
        cursor.execute("SELECT * FROM Prescription WHERE veterinaire = %s", (telephone,))
        prescriptions = cursor.fetchall()

        if prescriptions:
            for prescription in prescriptions:
                print({
                    'id': prescription[0],
                    'date_debut': prescription[1],
                    'duree': prescription[2],
                    'nom_molecule': prescription[3],
                    'quantite': prescription[4],
                    'animal': prescription[5],
                    'veterinaire': prescription[6]
                })
        else:
            print("Aucune prescription trouvée pour ce vétérinaire.")
    except psycopg2.Error as e:
        print(f"Erreur lors de la récupération des prescriptions: {e}")

def verifier_veterinaire(cursor, telephone):
    query_veto = "SELECT poste FROM Personnel WHERE telephone = %s AND poste = 'vétérinaire';"
    try:
        cursor.execute(query_veto, (telephone,))
        result = cursor.fetchone()
        return result is not None
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de la vérification du vétérinaire: {e}")
        return False
