import re  # module pour l'expression régulière
import psycopg2

def personnel(cursor):
    quitter = False
    while not quitter:
        print("1. Ajouter un personnel")
        print("2. Modifier un personnel")
        print("3. Supprimer un personnel")
        print("4. Retour au menu principal")
        choix = input("Choisissez une option : ")

        if choix == '1':
            # Ajouter un personnel
            telephone_ok = False
            while not telephone_ok:
                try:
                    # On vérifie si le numéro de téléphone existe à l'aide de la fonction
                    telephone = int(input("Entrez le numéro de téléphone : "))
                    telephone_ok = verifier_telephone(cursor, telephone)
                    if not telephone_ok:
                        print("Le numéro de téléphone existe déjà.")
                    else:
                        print("Le numéro de téléphone est disponible.")
                except ValueError:
                    print("Veuillez entrer un numéro valide.")
                except psycopg2.Error as e:
                    print(f"Une erreur de base de données s'est produite : {e}")
                    break
            nom = input("Entrez le nom : ")
            prenom = input("Entrez le prénom : ")
            date_naissance = input("Entrez la date de naissance (YYYY-MM-DD) : ")
            adresse = input("Entrez l'adresse : ")
            poste = input("Entrez le poste (vétérinaire/assistant) : ")
            specialite = input("Entrez la spécialité (mammifère/reptiles/oiseaux/autres) : ")

            if poste not in {'vétérinaire', 'assistant'}:
                print("Le poste doit être 'vétérinaire' ou 'assistant'.")
                continue
            if specialite not in {'mammifère', 'reptiles', 'oiseaux', 'autres'}:
                print("La spécialité doit être 'mammifère', 'reptiles', 'oiseaux' ou 'autres'.")
                continue
            if not verifier_date(date_naissance):
                print("Date de naissance invalide.")
                continue

            requete = f"""
            INSERT INTO Personne (telephone, nom, prenom, date_naissance, adresse)
            VALUES (%s, %s, %s, TO_DATE(%s, 'YYYY-MM-DD'), %s);
            INSERT INTO Personnel (telephone, poste, specialite)
            VALUES (%s, %s, %s);
            """
            try:
                cursor.execute(requete, (telephone, nom, prenom, date_naissance, adresse, telephone, poste, specialite))
                print("Personnel ajouté avec succès.")
            except psycopg2.Error as e:
                print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")

        elif choix == '2':
            # Modifier un personnel
            personnel_ok = False
            while not personnel_ok:
                try:
                    telephone = int(input("Entrez le numéro de téléphone du personnel : "))
                    personnel_ok = verifier_personnel(cursor, telephone)
                    if not personnel_ok:
                        print("Le personnel n'existe pas.")
                except ValueError:
                    print("Veuillez entrer un numéro de téléphone valide.")

            print("1. Modifier le poste")
            print("2. Modifier la spécialité")
            modif = input("Votre choix : ")

            if modif == '1':
                poste = input("Entrez le nouveau poste (vétérinaire/assistant) : ")
                if poste not in {'vétérinaire', 'assistant'}:
                    print("Le poste doit être 'vétérinaire' ou 'assistant'.")
                    continue
                requete = "UPDATE Personnel SET poste = %s WHERE telephone = %s;"
                try:
                    cursor.execute(requete, (poste, telephone))
                    print("Poste mis à jour avec succès.")
                except psycopg2.Error as e:
                    print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")

            elif modif == '2':
                specialite = input("Entrez la nouvelle spécialité (mammifère/reptiles/oiseaux/autres) : ")
                if specialite not in {'mammifère', 'reptiles', 'oiseaux', 'autres'}:
                    print("La spécialité doit être 'mammifère', 'reptiles', 'oiseaux' ou 'autres'.")
                    continue
                requete = "UPDATE Personnel SET specialite = %s WHERE telephone = %s;"
                try:
                    cursor.execute(requete, (specialite, telephone))
                    print("Spécialité mise à jour avec succès.")
                except psycopg2.Error as e:
                    print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")

        elif choix == '3':
            # Supprimer un personnel
            personnel_ok = False
            while not personnel_ok:
                try:
                    telephone = int(input("Entrez le numéro de téléphone du personnel à supprimer : "))
                    personnel_ok = verifier_personnel(cursor, telephone)
                    if not personnel_ok:
                        print("Le personnel n'existe pas.")
                except ValueError:
                    print("Veuillez entrer un numéro de téléphone valide.")

            try:
                cursor.execute("SELECT specialite FROM Personnel WHERE telephone = %s;", (telephone,))
                specialite = cursor.fetchone()[0]

                cursor.execute("SELECT telephone FROM Personnel WHERE specialite = %s AND telephone != %s;", (specialite, telephone))
                autres_personnels = cursor.fetchall()

                if autres_personnels:
                    nouveau_telephone = autres_personnels[0][0]
                    cursor.execute("UPDATE Animal SET personnel = %s WHERE personnel = %s;", (nouveau_telephone, telephone))
                    print(f"Les animaux du personnel {telephone} ont été réassignés au personnel {nouveau_telephone}.")
                else:
                    print("Aucun autre personnel avec la même spécialité trouvé pour la réassignation des animaux.")

                cursor.execute("DELETE FROM Personnel WHERE telephone = %s;", (telephone,))
                cursor.execute("DELETE FROM Personne WHERE telephone = %s;", (telephone,))
                print("Personnel supprimé avec succès.")
            except psycopg2.Error as e:
                print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")

        elif choix == '4':
            quitter = True
        else:
            print("Attention, choisissez 1, 2, 3 ou 4!")

def verifier_personnel(cursor, telephone):
    query = "SELECT * FROM Personnel WHERE telephone = %s;"
    try:
        cursor.execute(query, (telephone,))
        result = cursor.fetchone()
        return result is not None
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")
        return False

def verifier_date(date):
    pattern = re.compile(r'^\d{4}-\d{2}-\d{2}$')
    return bool(pattern.match(date))

def verifier_telephone(cursor, telephone):
    query_telephone = "SELECT * FROM Personne WHERE telephone = %s;"
    try:
        cursor.execute(query_telephone, (telephone,))
        result = cursor.fetchone()
        return result is None
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de l'exécution de la requête: {e}")
        return False
