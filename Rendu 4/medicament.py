import psycopg2

def creer_medicament(cursor):
    molecule = input("Entrez le nom du médicament : ")
    effets = input("Entrez la description du médicament : ")

    if not molecule or not effets:
        print("Nom et description sont requis!")
        return

    try:
        cursor.execute("INSERT INTO Medicament (molecule, effets) VALUES (%s, %s) RETURNING molecule", (molecule, effets))
        cursor.connection.commit()
        medicament_id = cursor.fetchone()[0]
        print(f"Médicament créé avec l'ID: {medicament_id}")
    except Exception as e:
        print(f"Erreur lors de la création du médicament: {e}")
        cursor.connection.rollback()

def verifier_molecule(cursor, molecule):
    query_molecule = "SELECT * FROM Medicament WHERE molecule = %s;"
    try:
        cursor.execute(query_molecule, (molecule,))
        result = cursor.fetchone()
        return result is not None
    except psycopg2.Error as e:
        print(f"Une erreur s'est produite lors de la vérification de la molécule: {e}")
        return False

def modifier_medicament(cursor):
    molecule = input("Entrez la molécule du médicament à modifier : ")

    # Check if the molecule exists
    if not verifier_molecule(cursor, molecule):
        print("La molécule n'existe pas.")
        return

    nom = input("Entrez le nouveau nom du médicament : ")
    effets = input("Entrez la nouvelle description du médicament : ")

    if not nom or not effets:
        print("Nom et description sont requis!")
        return

    try:
        cursor.execute("UPDATE Medicament SET molecule = %s, effets = %s WHERE molecule = %s", (nom, effets, molecule))
        cursor.connection.commit()
        print(f"Médicament avec l'ID {molecule} mis à jour")
    except Exception as e:
        print(f"Erreur lors de la mise à jour du médicament: {e}")
        cursor.connection.rollback()

def menu_medicaments(cursor):
    quitter = False
    while not quitter:
        print("\nActions sur les Médicaments:\n")
        print("1. Créer un médicament")
        print("2. Modifier un médicament")
        print("3. Retour au menu principal")
        choix = input("Choisissez une option : ")
        if choix == '1':
            creer_medicament(cursor)
        elif choix == '2':
            modifier_medicament(cursor)
        elif choix == '3':
            quitter = True
        else:
            print("Option invalide. Veuillez choisir une option valide.")
