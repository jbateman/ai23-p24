-- Drop tables with CASCADE to handle dependencies
DROP TABLE IF EXISTS Personne CASCADE;
DROP TABLE IF EXISTS Client CASCADE;
DROP TABLE IF EXISTS Specialite CASCADE;
DROP TABLE IF EXISTS Poste CASCADE;
DROP TABLE IF EXISTS Personnel CASCADE;
DROP TABLE IF EXISTS Animal CASCADE;
DROP TABLE IF EXISTS Medicament CASCADE;
DROP TABLE IF EXISTS Prescription CASCADE;

-- Drop types if they exist
DROP TYPE IF EXISTS SPECIALITE CASCADE;
DROP TYPE IF EXISTS POSTE CASCADE;

-- Create types
CREATE TYPE SPECIALITE_ENUM AS ENUM ('mammifère', 'reptile', 'oiseaux', 'autres');
CREATE TYPE POSTE_ENUM AS ENUM('vétérinaire', 'assistant');

-- Create table Personne
CREATE TABLE Personne (
    telephone INT PRIMARY KEY,
    nom VARCHAR(255) NOT NULL,
    prenom VARCHAR(255) NOT NULL,
    date_naissance DATE NOT NULL,
    adresse VARCHAR(255) NOT NULL
);

-- Create table Client
CREATE TABLE Client (
    telephone INT PRIMARY KEY,
    FOREIGN KEY (telephone) REFERENCES Personne(telephone)
);

-- Create table Specialite
CREATE TABLE Specialite (
    specialite SPECIALITE_ENUM PRIMARY KEY
);

-- Create table Poste
CREATE TABLE Poste (
    poste POSTE_ENUM PRIMARY KEY
);

-- Create table Personnel
CREATE TABLE Personnel (
    telephone INT PRIMARY KEY,
    specialite SPECIALITE_ENUM,
    poste POSTE_ENUM,
    FOREIGN KEY (specialite) REFERENCES Specialite(specialite),
    FOREIGN KEY (poste) REFERENCES Poste(poste),
    FOREIGN KEY (telephone) REFERENCES Personne(telephone)
);

-- Create table Animal
CREATE TABLE Animal (
    id_animal INT PRIMARY KEY,
    nom VARCHAR(255) NOT NULL,
    espece VARCHAR(255) NOT NULL,
    poids INT NOT NULL,
    taille INT NOT NULL,
    date_naissance DATE,
    proprietaire INT NOT NULL,
    personnel INT NOT NULL,
    specialite SPECIALITE_ENUM,
    FOREIGN KEY (proprietaire) REFERENCES Client(telephone),
    FOREIGN KEY (personnel) REFERENCES Personnel(telephone),
    FOREIGN KEY (specialite) REFERENCES Specialite(specialite),
    CONSTRAINT chk_no_self_treatment CHECK (proprietaire <> personnel)
);

-- Create table Medicament
CREATE TABLE Medicament (
    molecule VARCHAR(255) PRIMARY KEY,
    effets TEXT NOT NULL,
    specialite SPECIALITE_ENUM,
    FOREIGN KEY (specialite) REFERENCES Specialite(specialite)
);

-- Create table Prescription
CREATE TABLE Prescription (
    id_prescription INT PRIMARY KEY,
    date_debut DATE NOT NULL,
    duree INT NOT NULL,
    nom_molecule VARCHAR(255) NOT NULL,
    quantite INT NOT NULL,
    animal INT NOT NULL,
    veterinaire INT NOT NULL,
    FOREIGN KEY (nom_molecule) REFERENCES Medicament(molecule),
    FOREIGN KEY (animal) REFERENCES Animal(id_animal),
    FOREIGN KEY (veterinaire) REFERENCES Personnel(telephone)
);


-- Insert into Personne
INSERT INTO Personne (telephone, nom, prenom, date_naissance, adresse) VALUES
(1, 'Dupont', 'Jean', '1975-01-01', '123 Rue A'),
(2, 'Durand', 'Marie', '1980-02-02', '234 Rue B'),
(3, 'Martin', 'Pierre', '1985-03-03', '345 Rue C'),
(4, 'Bernard', 'Luc', '1990-04-04', '456 Rue D'),
(5, 'Thomas', 'Nina', '1995-05-05', '567 Rue E'),
(6, 'Petit', 'Anne', '2000-06-06', '678 Rue F'),
(7, 'Robert', 'Louis', '1977-07-07', '789 Rue G'),
(8, 'Richard', 'Sophie', '1983-08-08', '890 Rue H'),
(9, 'Leroy', 'Laura', '1992-09-09', '901 Rue I');

-- Insert into Client
INSERT INTO Client (telephone) VALUES
(1),
(2),
(3);

-- Insert into Specialite
INSERT INTO Specialite (specialite) VALUES
('mammifère'),
('reptile'),
('oiseaux'),
('autres');

-- Insert into Poste
INSERT INTO Poste (poste) VALUES
('vétérinaire'),
('assistant');

-- Insert into Personnel
INSERT INTO Personnel (telephone, specialite, poste) VALUES
(4, 'mammifère', 'vétérinaire'),
(5, 'reptile', 'vétérinaire'),
(6, 'oiseaux', 'vétérinaire'),
(7, 'autres', 'vétérinaire'),
(8, NULL, 'assistant'),
(9, NULL, 'assistant');

-- Insert into Animal
INSERT INTO Animal (id_animal, nom, espece, poids, taille, date_naissance, proprietaire, personnel, specialite) VALUES
(1, 'Lion', 'mammifère', 200, 120, '2010-01-01', 1, 4, 'mammifère'),
(2, 'Elephant', 'mammifère', 5000, 300, '2005-02-02', 1, 4, 'mammifère'),
(3, 'Chien', 'mammifère', 50, 70, '2012-03-03', 2, 4, 'mammifère'),
(4, 'Crocodile', 'reptile', 700, 150, '2008-04-04', 2, 5, 'reptile'),
(5, 'Serpent', 'reptile', 20, 200, '2013-05-05', 3, 5, 'reptile'),
(6, 'Lézard', 'reptile', 5, 50, '2016-06-06', 3, 5, 'reptile'),
(7, 'Aigle', 'oiseaux', 7, 100, '2011-07-07', 1, 6, 'oiseaux'),
(8, 'Perroquet', 'oiseaux', 1, 30, '2014-08-08', 2, 6, 'oiseaux'),
(9, 'Pigeon', 'oiseaux', 0.5, 20, '2018-09-09', 3, 6, 'oiseaux'),
(10, 'Tortue', 'autres', 100, 40, '2000-10-10', 1, 7, 'autres'),
(11, 'Poisson', 'autres', 1, 5, '2015-11-11', 2, 7, 'autres'),
(12, 'Lapin', 'autres', 2, 30, '2017-12-12', 3, 7, 'autres');

-- Insert into Medicament
INSERT INTO Medicament (molecule, effets, specialite) VALUES
('Ibuprofène', 'Anti-inflammatoire', 'mammifère'),
('Paracétamol', 'Antipyrétique', 'reptile'),
('Amoxicilline', 'Antibiotique', 'oiseaux'),
('Metronidazole', 'Antiparasitaire', 'autres');

-- Insert into Prescription
INSERT INTO Prescription (id_prescription, date_debut, duree, nom_molecule, quantite, animal, veterinaire) VALUES
(1, '2022-01-01', 10, 'Ibuprofène', 5, 1, 4),
(2, '2022-01-02', 15, 'Ibuprofène', 10, 2, 4),
(3, '2022-01-03', 7, 'Ibuprofène', 8, 3, 4),
(4, '2022-01-04', 14, 'Paracétamol', 6, 4, 5),
(5, '2022-01-05', 12, 'Paracétamol', 9, 5, 5),
(6, '2022-01-06', 9, 'Paracétamol', 4, 6, 5),
(7, '2022-01-07', 11, 'Amoxicilline', 7, 7, 6),
(8, '2022-01-08', 13, 'Amoxicilline', 5, 8, 6),
(9, '2022-01-09', 10, 'Amoxicilline', 3, 9, 6),
(10, '2022-01-10', 8, 'Metronidazole', 6, 10, 7),
(11, '2022-01-11', 14, 'Metronidazole', 7, 11, 7),
(12, '2022-01-12', 12, 'Metronidazole', 4, 12, 7);




-- Trigger to check that animal weight is not negative
CREATE OR REPLACE FUNCTION check_poids() RETURNS TRIGGER AS $$
BEGIN
    IF NEW.poids < 0 THEN
        RAISE EXCEPTION 'Le poids de l''animal ne peut pas être inférieur à 0.';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER check_poids_trigger
BEFORE INSERT OR UPDATE ON Animal
FOR EACH ROW EXECUTE FUNCTION check_poids();

-- Trigger to check that prescriptions are made by a veterinarian
CREATE OR REPLACE FUNCTION check_veterinaire_prescription() RETURNS TRIGGER AS $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM Personnel WHERE telephone = NEW.veterinaire AND poste = 'vétérinaire') THEN
        RAISE EXCEPTION 'Seul un vétérinaire peut prescrire un traitement.';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER check_veterinaire_prescription_trigger
BEFORE INSERT OR UPDATE ON Prescription
FOR EACH ROW EXECUTE FUNCTION check_veterinaire_prescription();
