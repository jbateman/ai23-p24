import psycopg2

def poids_taille_moyenne(cursor):
    quitter = False
    while not quitter:
        print("\nChoisissez une espèce:\n")
        print("1. mammifère")
        print("2. reptile")
        print("3. oiseaux")
        print("4. autres")
        print("5. Retour au menu principal")

        choix = input("Votre choix : ")

        espece_choices = {
            '1': 'mammifère',
            '2': 'reptile',
            '3': 'oiseaux',
            '4': 'autres'
        }

        if choix == '5':
            quitter = True
            continue

        espece = espece_choices.get(choix)

        if espece is None:
            print("Choix invalide. Veuillez choisir une option valide.")
            continue

        try:
            cursor.execute("SELECT poids, taille FROM Animal WHERE specialite = %s;", (espece,))
            animaux = cursor.fetchall()

            if not animaux:
                print(f"Aucun animal trouvé pour l'espèce {espece}.")
                continue

            total_poids = sum(animal[0] for animal in animaux)
            total_taille = sum(animal[1] for animal in animaux)
            count = len(animaux)

            moyenne_poids = total_poids / count
            moyenne_taille = total_taille / count

            print(f"La taille moyenne d'un {espece} est de {moyenne_taille:.2f} et leur poids moyen est de {moyenne_poids:.2f}")
        except psycopg2.Error as e:
            print(f"Erreur lors de la récupération des données: {e}")
