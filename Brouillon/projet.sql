-- Création de la table Personne
CREATE TABLE Personne (
    telephone INT PRIMARY KEY,
    nom VARCHAR(50) NOT NULL,
    prenom VARCHAR(50) NOT NULL,
    date_naissance DATE NOT NULL,
    adresse VARCHAR(100) NOT NULL
);

-- Création de la table Client, qui hérite de Personne
CREATE TABLE Client (
    telephone INT PRIMARY KEY,
    FOREIGN KEY (telephone) REFERENCES Personne(telephone)
);

-- Création de la table Personnel, qui hérite de Personne
CREATE TABLE Personnel (
    telephone INT PRIMARY KEY,
    poste ENUM('vétérinaire', 'assistant') NOT NULL,
    specialite ENUM('mammifère', 'reptiles', 'oiseaux', 'autres') NOT NULL,
    FOREIGN KEY (telephone) REFERENCES Personne(telephone)
);

-- Création de la table Animal
CREATE TABLE Animal (
    id_animal INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL,
    espece VARCHAR(50) NOT NULL,
    poids INT NOT NULL,
    taille INT NOT NULL,
    date_naissance DATE,
    type ENUM('mammifère', 'reptiles', 'oiseaux', 'autres') NOT NULL,
    proprietaire INT,
    personnel INT,
    FOREIGN KEY (proprietaire) REFERENCES Client(telephone),
    FOREIGN KEY (personnel) REFERENCES Personnel(telephone)
);

-- Création de la table Medicament
CREATE TABLE Medicament (
    molecule VARCHAR(50) PRIMARY KEY,
    effets TEXT NOT NULL,
    autorise ENUM('mammifère', 'reptiles', 'oiseaux', 'autres') NOT NULL
);

-- Création de la table Prescription
CREATE TABLE Prescription (
    id_prescription INT PRIMARY KEY AUTO_INCREMENT,
    date_debut DATE NOT NULL,
    duree INT NOT NULL,
    nom_molecule VARCHAR(50),
    quantite INT NOT NULL,
    id_animal INT,
    veterinaire INT,
    FOREIGN KEY (nom_molecule) REFERENCES Medicament(molecule),
    FOREIGN KEY (id_animal) REFERENCES Animal(id_animal),
    FOREIGN KEY (veterinaire) REFERENCES Personnel(telephone),
    CHECK ((SELECT poste FROM Personnel WHERE telephone = veterinaire) = 'vétérinaire')
);

-- Trigger pour vérifier la contrainte que seul un vétérinaire peut prescrire un traitement
DELIMITER //

CREATE TRIGGER BeforeInsertPrescription
BEFORE INSERT ON Prescription
FOR EACH ROW
BEGIN
    DECLARE poste VARCHAR(20);
    SELECT p.poste INTO poste FROM Personnel p WHERE p.telephone = NEW.veterinaire;
    IF poste != 'vétérinaire' THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Seul un vétérinaire peut prescrire un traitement';
    END IF;
END //

DELIMITER ;

-- Fonction pour calculer la date de fin du traitement
DELIMITER //

CREATE FUNCTION calculerDateFin(date_debut DATE, duree INT) RETURNS DATE
BEGIN
    RETURN DATE_ADD(date_debut, INTERVAL duree DAY);
END //

DELIMITER ;

-- Indexes pour optimiser les recherches
CREATE INDEX idx_animal_proprietaire ON Animal(proprietaire);
CREATE INDEX idx_prescription_animal ON Prescription(id_animal);
CREATE INDEX idx_prescription_veterinaire ON Prescription(veterinaire);

-- Vue pour la quantité totale de chaque type de médicament prescrit pour un animal donné
CREATE VIEW V_Quantite_Medicament_Par_Animal AS
SELECT 
    a.id_animal,
    a.nom AS animal_nom,
    m.molecule,
    SUM(p.quantite) AS total_quantite
FROM 
    Prescription p
JOIN 
    Animal a ON p.id_animal = a.id_animal
JOIN 
    Medicament m ON p.nom_molecule = m.molecule
GROUP BY 
    a.id_animal, m.molecule;

-- Vue pour les poids et tailles moyens des animaux d'une espèce traités
CREATE VIEW V_Poids_Taille_Moyens AS
SELECT 
    espece,
    AVG(poids) AS poids_moyen,
    AVG(taille) AS taille_moyenne
FROM 
    Animal
GROUP BY 
    espece;
